import torch
import math
import matplotlib.pyplot as plt

max_seq_len = 256
d_model = 256

pe = torch.zeros(max_seq_len, d_model, dtype=torch.float32)
position = torch.arange(0, max_seq_len, dtype=torch.float32).unsqueeze(1)
div_term = torch.exp(torch.arange(0, d_model, 2, dtype=torch.float32) *
                     -(math.log(10000.0) / d_model))
pe[:, 0::2] = torch.sin(position * div_term)
pe[:, 1::2] = torch.cos(position * div_term)

plt.imshow(pe, cmap='coolwarm', interpolation='nearest')
plt.show()
print(pe)
