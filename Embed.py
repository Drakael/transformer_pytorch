import torch
import torch.nn as nn
import math
from torch.autograd import Variable


class Embedder(nn.Module):
    def __init__(self, vocab_size, d_model):
        super().__init__()
        self.d_model = d_model
        self.embed = nn.Embedding(vocab_size, d_model)

    def forward(self, x):
        # return self.embed(x)
        out = self.embed(x) * math.sqrt(self.d_model)
        # print('embed.mean()', out.mean(), 'embed.std()', out.std())
        return out


class PositionalEncoder(nn.Module):
    def __init__(self, d_model, max_seq_len=150, dropout=0.1):
        super().__init__()
        self.d_model = d_model
        self.dropout = nn.Dropout(dropout)
        # create constant 'pe' matrix with values dependant on
        # pos and i
        pe = torch.zeros(max_seq_len, d_model, dtype=torch.float32)
        pe.require_grad = False
        position = torch.arange(0, max_seq_len, dtype=torch.float32).unsqueeze(1)
        div_term = (torch.arange(0, d_model, 2).float() * -(math.log(10000.0) / d_model)).exp()
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)
        self.register_buffer('pe', pe)

    def forward(self, x):
        # make embeddings relatively larger
        # x = x * math.sqrt(self.d_model)
        # add constant to embedding
        seq_len = x.size(1)
        pe = Variable(self.pe[:, :seq_len], requires_grad=False)
        # pe dimensions are 1 * seq_len * d_model
        if x.is_cuda:
            pe.cuda()
        x = x + pe
        return self.dropout(x)
