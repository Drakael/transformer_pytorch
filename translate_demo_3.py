import argparse
import time
import torch
from Models import get_model
from Process import *
from Beam import beam_search
from torch.autograd import Variable
import numpy as np
from Tokenize import tokenize
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib as mpl


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-load_weights', default="weights_3")
    parser.add_argument('-k', type=int, default=6)
    parser.add_argument('-max_len', type=int, default=200)
    parser.add_argument('-d_model', type=int, default=512)
    parser.add_argument('-n_layers', type=int, default=6)
    # parser.add_argument('-src_data', required=True)
    # parser.add_argument('-trg_data', required=True)
    parser.add_argument('-max_strlen', type=int, default=200)
    parser.add_argument('-batchsize', type=int, default=2200)
    parser.add_argument('-src_lang', default="en")
    parser.add_argument('-trg_lang', default="fr")
    parser.add_argument('-heads', type=int, default=8)
    parser.add_argument('-dropout', type=int, default=0.11)
    parser.add_argument('-no_cuda', action='store_true', default=False)
    parser.add_argument('-floyd', action='store_true')
    opt = parser.parse_args()
    opt.device = 0 if opt.no_cuda is False else -1

    assert opt.k > 0
    assert opt.max_len > 10

    SRC, TRG = create_fields(opt)
    model = get_model(opt, len(SRC.vocab), len(TRG.vocab))
    model.eval()
    t_src = tokenize(opt.src_lang)
    # t_trg = tokenize(opt.trg_lang)
    src_file = open('data/english.txt', "r", encoding="utf-8")
    examples = src_file.readlines()

    plt.ion()
    plt.show()
    # fig, ax = plt.subplots(1, 1, figsize=(15, 4))
    fig = plt.figure(figsize=(15, 4))
    ax = fig.add_subplot(1, 1, 1)
    mpl.style.use('seaborn')
    while True:
    # for i in range(10):
        sent = t_src.tokenizer(examples[np.random.randint(len(examples))])
        src = torch.LongTensor([[SRC.vocab.stoi[w] for w in sent]])
        src = Variable(src, requires_grad=False)
        if opt.device == 0:
            src = src.cuda()
        # src_mask = (src != SRC.vocab.stoi["<pad>"]).unsqueeze(-2).cuda()
        out = beam_search(src, model, SRC, TRG, opt)
        print(sent)
        if out != '':
            print('->', out)
            plot_attention(model, sent, out, fig, ax)
        else:
            print('empty translation')


def plot_attention(model, sent, out, fig, ax):
    tgt_sent = out.split()
    font = FontProperties()
    font.set_size('medium')
    font.set_family('sans-serif')
    alignment_right = {'horizontalalignment': 'right'}
    alignment_left = {'horizontalalignment': 'left'}
    # mpl.style.use('seaborn')
    src = sent
    trg = out.split()
    letter_spacing = 3.5
    word_spacing = 3.6
    y_start = 0.9
    col_spacing = 80
    spacing = 10.0
    offset_x = spacing - 1.0
    offset_y = 0.005
    offset_windows = 1.7*col_spacing+5*spacing
    xs = [-2*col_spacing, -col_spacing, 0, col_spacing,
          2*col_spacing, 3*col_spacing]
    array = np.zeros(model.encoder.layers[0].attn.scores[0, 2:4].detach().cpu().numpy().mean(axis=0)[:len(tgt_sent), :len(sent)].shape)
    for i in range(0, 6):
        array += model.encoder.layers[i].attn.scores[0, 2:4].detach().cpu().numpy().mean(axis=0)[:len(tgt_sent), :len(sent)]
    print('array.shape', array.shape)
    array_mean = array.mean(axis=0)
    array_mean -= np.min(array_mean)*0.618
    array_mean /= np.max(array_mean)
    print('array_mean.shape', array_mean.shape)
    cnt = 0
    for i, w in enumerate(src):
        t = plt.text(-170 + cnt * letter_spacing + i * word_spacing, 1.15, w,
                     fontproperties=font, **alignment_left)
        print(array_mean[i])
        t.set_bbox(dict(facecolor='red', alpha=array_mean[i], edgecolor='white'))
        cnt += len(w)
    array = np.zeros(model.decoder.layers[0].attn_2.scores[0, 3:5].detach().cpu().numpy().mean(axis=0)[:len(tgt_sent), :len(sent)].shape)
    for i in range(0, 6):
        array += model.decoder.layers[i].attn_2.scores[0, 3:5].detach().cpu().numpy().mean(axis=0)[:len(tgt_sent), :len(sent)]
    print('array.shape', array.shape)
    array_mean = array.mean(axis=1)
    array_mean -= np.min(array_mean)
    array_mean /= np.max(array_mean)
    print('array_mean.shape', array_mean.shape)
    cnt = 0
    for i, w in enumerate(trg):
        t = plt.text(-170 + cnt * letter_spacing + i * word_spacing, 1.05, w,
                     fontproperties=font, **alignment_left)
        print(array_mean[i])
        t.set_bbox(dict(facecolor='red', alpha=array_mean[i], edgecolor='white'))
        cnt += len(w)
    for i in range(0, 6):
        array = model.decoder.layers[i].attn_2.scores[0, :].detach().cpu().numpy().max(axis=0)[:len(tgt_sent), :len(sent)]
        # print(array.shape, '\n', array)
        for k, w in enumerate(src):
            t = plt.text(-spacing + xs[i], y_start - k * 0.1, w,
                         fontproperties=font, **alignment_right)
        for k, w in enumerate(trg):
            t = plt.text(spacing + xs[i], y_start - k * 0.1, w,
                         fontproperties=font, **alignment_left)
        for row in range(0, array.shape[0]):
            array[row, :] -= np.min(array[row, :])
            array[row, :] /= np.max(array[:, :])
            for col in range(0, array.shape[1]):
                l = Line2D([-offset_x + xs[i], offset_x + xs[i]],
                           [y_start - col * 0.1 + offset_y,
                            y_start - row * 0.1 + offset_y],
                           alpha=array[row, col]**1.2,
                           color='C'+str(i))
                ax.add_line(l)
    # print('ax.lines', ax.lines)
    plt.axis([-col_spacing-offset_windows,
              col_spacing+offset_windows,
              y_start - (max(len(src), len(trg)) * 0.1), 1.2])
    plt.axis('off')
    line0 = Line2D([], [], color='C0', label='Layer 1')
    line1 = Line2D([], [], color='C1', label='Layer 2')
    line2 = Line2D([], [], color='C2', label='Layer 3')
    line3 = Line2D([], [], color='C3', label='Layer 4')
    line4 = Line2D([], [], color='C4', label='Layer 5')
    line5 = Line2D([], [], color='C5', label='Layer 6')
    plt.legend(handles=[line0, line1, line2, line3, line4, line5], loc=3)
    plt.show()
    plt.pause(30)
    ax.cla()


if __name__ == '__main__':
    main()
