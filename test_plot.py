from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt

plt.subplot(111, facecolor='w')
font = FontProperties()
font.set_size('large')
font.set_family('sans-serif')
alignment_right = {'horizontalalignment': 'right', 'verticalalignment': 'baseline'}
alignment_left = {'horizontalalignment': 'left', 'verticalalignment': 'baseline'}
# t = plt.text(-0.8, 0.9, 'family', fontproperties=font,
#          **alignment_left)

yp = [0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2]
y_start = 0.9

src = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace', 'serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
trg = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace', 'serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
for k, w in enumerate(src):
    t = plt.text(-0.3, y_start - k * 0.1, w, fontproperties=font,
                 **alignment_right)
for k, w in enumerate(trg):
    t = plt.text(0.3, y_start - k * 0.1, w, fontproperties=font,
                 **alignment_left)

plt.axis([-1, 1, y_start - (max(len(src), len(trg)) * 0.1), 1])
plt.axis('off')
plt.show()
