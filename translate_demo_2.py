import argparse
import time
import torch
from Models import get_model
from Process import *
import torch.nn.functional as F
from Optim import CosineWithRestarts
from Batch import create_masks
import pdb
import dill as pickle
# import pickle
import argparse
from Models import get_model
from Beam import beam_search
from nltk.corpus import wordnet
from torch.autograd import Variable
import re
import numpy as np
from Batch import nopeak_mask
import seaborn
from Tokenize import tokenize
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib as mpl


def get_synonym(word, SRC):
    syns = wordnet.synsets(word)
    for s in syns:
        for l in s.lemmas():
            if SRC.vocab.stoi[l.name()] != 0:
                return SRC.vocab.stoi[l.name()]
    return 0


def multiple_replace(dict, text):
    # Create a regular expression  from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))

    # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)


def translate_sentence(sentence, model, opt, SRC, TRG):
    model.eval()
    indexed = []
    sentence = SRC.preprocess(sentence)
    for tok in sentence:
        if SRC.vocab.stoi[tok] != 0 or opt.floyd is True:
            indexed.append(SRC.vocab.stoi[tok])
        else:
            indexed.append(get_synonym(tok, SRC))
    sentence = Variable(torch.LongTensor([indexed]))
    if opt.device == 0:
        sentence = sentence.cuda()

    sentence = beam_search(sentence, model, SRC, TRG, opt)

    return multiple_replace({' ?': '?', ' !': '!', ' .': '.', '\' ': '\'',
                             ' ,': ','}, sentence)


def translate(opt, model, SRC, TRG):
    sentences = opt.text.lower().split('.')
    translated = []

    for sentence in sentences:
        translated.append(translate_sentence(sentence + '.', model, opt,
                                             SRC, TRG).capitalize())

    return (' '.join(translated))


def subsequent_mask(size):
    "Mask out subsequent positions."
    attn_shape = (1, size, size)
    subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
    return torch.from_numpy(subsequent_mask) == 0


def greedy_decode(model, src, src_mask, opt, max_len, start_symbol):
    print('src.dtype', src.dtype)
    print('src_mask.dtype', src_mask.dtype)
    e_output = model.encoder(src, src_mask).long()
    ys = torch.ones(1, 1).fill_(start_symbol).type_as(src.data)
    for i in range(max_len-1):
        print('e_output.dtype', e_output.dtype)
        print('src_mask.dtype', src_mask.dtype)
        print('ys.dtype', ys.dtype)
        print('src.data.dtype', src.data.dtype)

        outputs = torch.LongTensor([[start_symbol]]).cuda()
        if opt.device == 0:
            outputs = outputs
        trg_mask = nopeak_mask(1, opt)
        # out = model.decoder(e_output, src_mask,
        #                     Variable(ys),
        #                     Variable(subsequent_mask(ys.size(1))
        #                              .type_as(src.data)))
        print('outputs.dtype', outputs.dtype)
        print('e_output.dtype', e_output.dtype)
        print('src_mask.dtype', src_mask.dtype)
        print('trg_mask.dtype', trg_mask.dtype)
        out = model.out(model.decoder(outputs,
                        e_output, src_mask, trg_mask))
        out = F.softmax(out, dim=-1)
        probs, ix = out[:, -1].data.topk(opt.k)
        # prob = model.generator(out[:, -1])
        print('ix[0]', ix[0])
        _, next_word = torch.max(probs, dim=1)
        next_word = next_word.data[0]
        ys = torch.cat([ys,
                        torch.ones(1, 1).type_as(src.data).fill_(next_word)],
                       dim=1)
    return ys


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-load_weights', default="weights_3")
    parser.add_argument('-k', type=int, default=6)
    parser.add_argument('-max_len', type=int, default=200)
    parser.add_argument('-d_model', type=int, default=512)
    parser.add_argument('-n_layers', type=int, default=6)
    # parser.add_argument('-src_data', required=True)
    # parser.add_argument('-trg_data', required=True)
    parser.add_argument('-max_strlen', type=int, default=200)
    parser.add_argument('-batchsize', type=int, default=2200)
    parser.add_argument('-src_lang', default="en")
    parser.add_argument('-trg_lang', default="fr")
    parser.add_argument('-heads', type=int, default=8)
    parser.add_argument('-dropout', type=int, default=0.11)
    parser.add_argument('-no_cuda', action='store_true')
    parser.add_argument('-floyd', action='store_true')

    opt = parser.parse_args()

    opt.device = 0 if opt.no_cuda is False else -1

    assert opt.k > 0
    assert opt.max_len > 10

    SRC, TRG = create_fields(opt)
    model = get_model(opt, len(SRC.vocab), len(TRG.vocab))
    model.eval()
    t_src = tokenize(opt.src_lang)
    t_trg = tokenize(opt.trg_lang)



    src_file = open('data/english.txt', "r", encoding="utf-8")
    examples = src_file.readlines()

    plt.ion()
    plt.show()
    # fig, ax = plt.subplots(1, 1, figsize=(15, 4))
    fig = plt.figure(figsize=(15, 4))
    ax = fig.add_subplot(1, 1, 1)
    mpl.style.use('seaborn')
    for i in range(1):
        sent = t_src.tokenizer(examples[np.random.randint(len(examples))])

        src = torch.LongTensor([[SRC.vocab.stoi[w] for w in sent]])
        src = Variable(src, requires_grad=False).cuda()
        src_mask = (src != SRC.vocab.stoi["<pad>"]).unsqueeze(-2).cuda()
        out = beam_search(src, model, SRC, TRG, opt)
        print(sent)
        print(out)
        plot_attention(model, sent, out, fig, ax)

def plot_attention(model, sent, out, fig, ax):
    tgt_sent = out.split()
    font = FontProperties()
    font.set_size('large')
    font.set_family('sans-serif')
    alignment_right = {'horizontalalignment': 'right'}
    alignment_left = {'horizontalalignment': 'left'}
    # mpl.style.use('seaborn')
    y_start = 0.9

    src = sent
    trg = out.split()

    for i in range(0, 6):
        xs = [-12, -6, 0, 6, 12, 18]
        array = model.decoder.layers[i].attn_2.scores[0, :].detach().cpu().numpy().max(axis=0)[:len(tgt_sent), :len(sent)]
        print(array.shape, '\n', array)
        for k, w in enumerate(src):
            t = plt.text(-1.0 + xs[i], y_start - k * 0.1, w, fontproperties=font,
                         **alignment_right)
        for k, w in enumerate(trg):
            t = plt.text(1.0 + xs[i], y_start - k * 0.1, w, fontproperties=font,
                         **alignment_left)
        for row in range(0, array.shape[0]):
            array[row, :] -= np.min(array[row, :])
            array[row, :] /= np.max(array[:, :])
            for col in range(0, array.shape[1]):
                l = Line2D([-0.9 + xs[i], 0.9 + xs[i]],
                                  [y_start - col * 0.1, y_start - row * 0.1],
                                  alpha=array[row, col]**1.2,
                                  color='C'+str(i))
                ax.add_line(l)
    print('ax.lines', ax.lines)


    plt.axis([-17, 21, y_start - (max(len(src), len(trg)) * 0.1), 1])
    plt.axis('off')

    line0 = Line2D([], [], color='C0', label='Layer 1')
    line1 = Line2D([], [], color='C1', label='Layer 2')
    line2 = Line2D([], [], color='C2', label='Layer 3')
    plt.legend(handles=[line0, line1, line2], loc=2)
    ax.set_visible(True)
    plt.draw()
    plt.pause(30)
    ax.cla()
    # sleep(10)
    # for i, line in enumerate(ax.lines):
    #     # ax.lines.pop(i)
    #     ax.lines.remove(line)
    # print('ax', vars(ax), dir(ax))
    print('ax.lines', ax.lines)
    fig.clf()


if __name__ == '__main__':
    main()
