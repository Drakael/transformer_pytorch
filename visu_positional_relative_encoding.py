import torch
import math
import matplotlib.pyplot as plt

max_seq_len = 128
d_model = 128

# pe = torch.zeros(max_seq_len, d_model, dtype=torch.float32)
# position = torch.arange(0, max_seq_len, dtype=torch.float32).unsqueeze(1)
# div_term = torch.exp(torch.arange(0, d_model, 2, dtype=torch.float32) *
#                      -(math.log(10000.0) / d_model))
# pe[:, 0::2] = torch.sin(position * div_term)
# pe[:, 1::2] = torch.cos(position * div_term)


inv_freq = 1 / (10000 ** (torch.arange(0.0, d_model, 2.0) / d_model))
# register_buffer('inv_freq', inv_freq)


pos_seq = torch.arange(0.0, max_seq_len)
bsz = None
sinusoid_inp = torch.ger(pos_seq, inv_freq)
pos_emb = torch.cat([sinusoid_inp.sin(), sinusoid_inp.cos()], dim=-1)

print('pos_emb.size()', pos_emb.size())
if bsz is not None:
    pe = pos_emb[:, None, :].expand(-1, bsz, -1)
else:
    pe = pos_emb[:, None, :]

print('pe.size()', pe.size())

plt.imshow(pos_emb, cmap='coolwarm', interpolation='nearest')
plt.show()
print(pe)
