import argparse
from time import time
import torch
from Models import get_model
from Process import *
import torch.nn.functional as F
from Optim import CosineWithRestarts
from Batch import create_masks
import dill as pickle
import numpy as np
import sys
import json
# import pickle


def cal_loss(pred, gold, smoothing, padding_idx):
    ''' Calculate cross entropy loss, apply label smoothing if needed. '''
    gold = gold.contiguous().view(-1)

    if smoothing > 0.0:
        eps = smoothing
        n_class = pred.size(1)

        one_hot = torch.zeros_like(pred).scatter(1, gold.view(-1, 1), 1)
        one_hot = (one_hot * (1 - eps)) + ((1 - one_hot) * eps / (n_class - 1))
        log_prb = F.log_softmax(pred, dim=1)

        non_pad_mask = gold.ne(padding_idx)
        loss = -(one_hot * log_prb).sum(dim=1)
        loss = loss.masked_select(non_pad_mask).sum()  # average later
    else:
        loss = F.cross_entropy(pred, gold, ignore_index=padding_idx, reduction='sum')

    return loss


def train_model(model, opt, trg_vocab_size):
    print("training model...")
    model.train()
    start = time()
    if opt.checkpoint > 0:
        cptime = time()

    for epoch in range(opt.epochs):

        total_loss = 0
        avg_loss = 0
        if opt.floyd is False:
            print("   %dm: epoch %d [%s]  %d%%  loss = %s" %
                  ((time() - start)//60, epoch + 1, "".join(' '*20)
                   , 0, '...'),
                  end='\r')

        if opt.checkpoint > 0:
            torch.save(model.state_dict(), '{}/model_weights'.format(opt.load_weights or 'weights'))

        for i, batch in enumerate(opt.train):

            src = batch.src.transpose(0, 1)
            trg = batch.trg.transpose(0, 1)
            trg_input = trg[:, :-1]
            src_mask, trg_mask = create_masks(src, trg_input, opt)
            preds = model(src, trg_input, src_mask, trg_mask)
            ys = trg[:, 1:].contiguous().view(-1)
            opt.optimizer.zero_grad()

            # print('padding_idx', padding_idx)
            # print('opt.trg_pad', opt.trg_pad)
            # criterion = LabelSmoothing(size=trg_vocab_size, padding_idx=opt.trg_pad, smoothing=0.1)
            # print('preds', preds.contiguous().view(-1, preds.size(-1)).size())
            # print('targets', ys.size())
            # loss = criterion(preds.contiguous().view(-1, preds.size(-1)), ys)
            loss = cal_loss(preds.contiguous().view(-1, preds.size(-1)), ys, 0.1, opt.trg_pad)
            # loss = F.cross_entropy(preds.view(-1, preds.size(-1)), ys,
            #                        ignore_index=opt.trg_pad, reduction='sum')
            loss.backward()
            opt.optimizer.step()
            if opt.SGDR is True:
                opt.sched.step()

            total_loss += loss.item()

            if (i + 1) % opt.printevery == 0:
                p = int(100 * (i + 1) / opt.train_len)
                avg_loss = total_loss/opt.printevery
                if opt.floyd is False:
                    print("   %dm: epoch %d [%s%s]  %d%%  loss = %.3f" %\
                    ((time() - start)//60, epoch + 1, "".join('#'*(p//5)), "".join(' '*(20-(p//5))), p, avg_loss), end='\r')
                else:
                    print("   %dm: epoch %d [%s%s]  %d%%  loss = %.3f" %\
                    ((time() - start)//60, epoch + 1, "".join('#'*(p//5)), "".join(' '*(20-(p//5))), p, avg_loss))
                total_loss = 0

            # if opt.checkpoint > 0 and ((time()-cptime)//60) // opt.checkpoint >= 1:
            if opt.checkpoint > 0 and (epoch + 1) % opt.checkpoint == 0:
                torch.save(model.state_dict(), '{}/model_weights_e{}'.format(opt.load_weights or 'weights', epoch + 1))
                cptime = time()

        print("%dm: epoch %d [%s%s]  %d%%  loss = %.3f\nepoch %d complete, loss = %.03f" %
            ((time() - start)//60, epoch + 1, "".join('#'*(100//5)), "".join(' '*(20-(100//5))), 100, avg_loss, epoch + 1, avg_loss))


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-src_data', default='data/english.txt')
    parser.add_argument('-trg_data', default='data/french.txt')
    parser.add_argument('-src_lang', default='en')
    parser.add_argument('-trg_lang', default='fr')
    parser.add_argument('-no_cuda', action='store_true')
    parser.add_argument('-SGDR', action='store_true')
    parser.add_argument('-epochs', type=int, default=20)
    parser.add_argument('-n_layers', type=int, default=6)
    parser.add_argument('-d_model', type=int, default=768)
    parser.add_argument('-heads', type=int, default=12)
    parser.add_argument('-dropout', type=int, default=0.11)
    parser.add_argument('-batchsize', type=int, default=2200)
    parser.add_argument('-printevery', type=int, default=100)
    parser.add_argument('-lr', type=int, default=0.0001)
    parser.add_argument('-load_weights', default='weights_768dim')
    parser.add_argument('-create_valset', action='store_true')
    parser.add_argument('-max_strlen', type=int, default=200)
    parser.add_argument('-floyd', action='store_true')
    parser.add_argument('-checkpoint', type=int, default=20)

    opt = parser.parse_args()

    opt.device = 0 if opt.no_cuda is False else -1
    if opt.device == 0:
        assert torch.cuda.is_available()
    print('device', opt.device)

    read_data(opt)
    SRC, TRG = create_fields(opt)
    opt.train = create_dataset(opt, SRC, TRG)
    model = get_model(opt, len(SRC.vocab), len(TRG.vocab))
    print('SRC vocab size', len(SRC.vocab))
    print('TRG vocab size', len(TRG.vocab))

    # vocab_sizes = {'src_vocab_size': len(SRC.vocab), 'trg_vocab_size': len(TRG.vocab)}
    # with open('vocab_sizes.txt', 'w') as outfile:
    #     json.dump(vocab_sizes, outfile)

    opt.optimizer = torch.optim.Adam(model.parameters(), lr=opt.lr,
                                     betas=(0.9, 0.98), eps=1e-9,
                                     amsgrad=True)
    if opt.SGDR is True:
        print('opt.train_len', opt.train_len)
        opt.sched = CosineWithRestarts(opt.optimizer, T_max=opt.train_len)
        import matplotlib.pyplot as plt
        plt.plot(np.arange(1, 20000), [opt.sched.get_lr() for i in range(1, 20000)])
        plt.show()
        sys.exit('die')

    if opt.checkpoint > 0:
        print("model weights will be saved every {:d} epochs "
              "to directory {}/".format(opt.checkpoint, opt.load_weights or 'weights'))

    if opt.load_weights is not None and opt.floyd is not None:
        if not os.path.isdir(opt.load_weights):
            os.mkdir(opt.load_weights)
        with open('{}/SRC.pkl'.format(opt.load_weights), 'wb') as file:
            pickle.dump(SRC, file)
        with open('{}/TRG.pkl'.format(opt.load_weights), 'wb') as file:
            pickle.dump(TRG, file)

    train_model(model, opt, len(TRG.vocab))

    if opt.floyd is False:
        promptNextAction(model, opt, SRC, TRG)


def yesno(response):
    while True:
        if response != 'y' and response != 'n':
            response = input('command not recognised, enter y or n : ')
        else:
            return response


def promptNextAction(model, opt, SRC, TRG):
    saved_once = 1 if opt.load_weights is not None or opt.checkpoint > 0 else 0
    if opt.load_weights is not None:
        dst = opt.load_weights
    if opt.checkpoint > 0:
        dst = 'weights'
    while True:
        save = yesno(input('training complete, save results? [y/n] : '))
        if save == 'y':
            while True:
                if saved_once != 0:
                    res = yesno("save to same folder? [y/n] : ")
                    if res == 'y':
                        break
                dst = input('enter folder name to create for weights (no spaces) : ')
                if ' ' in dst or len(dst) < 1 or len(dst) > 30:
                    dst = input("name must not contain spaces and be between \
                                 1 and 30 characters length, enter again : ")
                else:
                    try:
                        os.mkdir(dst)
                    except:
                        res = yesno(input(dst + " already exists, use anyway? [y/n] : "))
                        if res == 'n':
                            continue
                    break

            print("saving weights to " + dst + "/...")
            torch.save(model.state_dict(), '{}/model_weights'.format(dst))
            if saved_once == 0:
                # pickle.dump(SRC, open('{}/SRC.pkl'.format(dst), 'wb'))
                # pickle.dump(TRG, open('{}/TRG.pkl'.format(dst), 'wb'))
                with open('{}/SRC.pkl'.format(dst), 'wb') as file:
                    pickle.dump(SRC, file)
                with open('{}/TRG.pkl'.format(dst), 'wb') as file:
                    pickle.dump(TRG, file)
                saved_once = 1

            print("weights and field pickles saved to " + dst)

        res = yesno(input("train for more epochs? [y/n] : "))
        if res == 'y':
            while True:
                epochs = input("type number of epochs to train for : ")
                try:
                    epochs = int(epochs)
                except:
                    print("input not a number")
                    continue
                if epochs < 1:
                    print("epochs must be at least 1")
                    continue
                else:
                    break
            opt.epochs = epochs
            train_model(model, opt)
        else:
            print("exiting program...")
            break

    # for asking about further training use while true loop, and return
if __name__ == "__main__":
    main()
