import pandas as pd
import torchtext
from torchtext import data
from Tokenize import tokenize
from Batch import MyIterator, batch_size_fn
import os
import dill as pickle
from sys import exit
import torch
from time import sleep, time


def read_data(opt, encoding='utf8'):
    if opt.src_data is not None:
        try:
            opt.src_data = open(opt.src_data, encoding=encoding).read().strip().split('\n')
        except:
            print("error: '" + opt.src_data + "' file not found")
            exit()
    if opt.trg_data is not None:
        try:
            opt.trg_data = open(opt.trg_data, encoding=encoding).read().strip().split('\n')
        except:
            print("error: '" + opt.trg_data + "' file not found")
            exit()


def create_fields(opt):
    spacy_langs = ['en', 'fr', 'de', 'es', 'pt', 'it', 'nl']
    if opt.src_lang not in spacy_langs:
        print('invalid src language: ' + opt.src_lang + 'supported languages : ' + spacy_langs)
    if opt.trg_lang not in spacy_langs:
        print('invalid trg language: ' + opt.trg_lang + 'supported languages : ' + spacy_langs)

    print("loading spacy tokenizers...")

    t_src = tokenize(opt.src_lang)
    t_trg = tokenize(opt.trg_lang)

    trg = data.Field(lower=True, tokenize=t_trg.tokenizer, init_token='<sos>',
                     eos_token='<eos>')
    src = data.Field(lower=True, tokenize=t_src.tokenizer)

    if opt.load_weights is not None:
        try:
            print("loading presaved fields...")
            with open('{}/SRC.pkl'.format(opt.load_weights), 'rb') as src_f:
                src = pickle.load(src_f)
            print('SRC loaded')
            with open('{}/TRG.pkl'.format(opt.load_weights), 'rb') as trg_f:
                trg = pickle.load(trg_f)
            print('TRG loaded')
        except:
            print("no SRC.pkl or TRG.pkl field files in " + opt.load_weights + "/")
    return (src, trg)


def create_dataset(opt, src, trg):

    print("creating dataset and iterator... ")

    raw_data = {'src': [line for line in opt.src_data], 'trg': [line for line in opt.trg_data]}
    df = pd.DataFrame(raw_data, columns=["src", "trg"])

    max_len_src = df['src'].str.count(' ').max()
    max_len_trg = df['trg'].str.count(' ').max()
    print('max_len_src', max_len_src, 'max_len_trg', max_len_trg)
    mean_len_src = df['src'].str.count(' ').mean()
    mean_len_trg = df['trg'].str.count(' ').mean()
    print('mean_len_src', mean_len_src, 'mean_len_trg', mean_len_trg)
    mask = (df['src'].str.count(' ') < opt.max_strlen) & (df['trg'].str.count(' ') < opt.max_strlen)
    df = df.loc[mask]
    print('df size', df['src'].size)
    print('saving as temporary csv')
    df.to_csv("translate_transformer_temp.csv", index=False)

    data_fields = [('src', src), ('trg', trg)]
    print('making TabularDataset')
    t0 = time()
    train = data.TabularDataset('./translate_transformer_temp.csv', format='csv', fields=data_fields)
    print('done in {:.2f}s'.format(time() - t0))
    print('making iterator')
    t0 = time()
    train_iter = MyIterator(train, batch_size=opt.batchsize, device=torch.device('cuda', opt.device),
                            repeat=False, sort_key=lambda x: (len(x.src), len(x.trg)),
                            batch_size_fn=batch_size_fn, train=True, shuffle=True)
    print('done in {:.2f}s'.format(time() - t0))
    print('deleting temporary csv')
    os.remove('translate_transformer_temp.csv')
    t0 = time()
    print('building vocab')
    src.build_vocab(train)
    trg.build_vocab(train)
    print('done in {:.2f}s'.format(time() - t0))
    if opt.load_weights is None:
        if opt.checkpoint > 0:
            try:
                os.mkdir("weights")
            except:
                print("weights folder already exists, run program with -load_weights weights to load them")
                exit()
            with open('weights/SRC.pkl', 'wb') as file:
                pickle.dump(src, file)
            with open('weights/TRG.pkl', 'wb') as file:
                pickle.dump(trg, file)

    opt.src_pad = src.vocab.stoi['<pad>']
    opt.trg_pad = trg.vocab.stoi['<pad>']

    opt.train_len = get_len(train_iter)
    print('opt.train_len', opt.train_len)

    return train_iter


def get_len(train):
    for i, b in enumerate(train):
        pass
    return i
