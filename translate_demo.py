import argparse
import time
import torch
from Models import get_model
from Process import *
import torch.nn.functional as F
from Optim import CosineWithRestarts
from Batch import create_masks
import pdb
import dill as pickle
# import pickle
import argparse
from Models import get_model
from Beam import beam_search
from nltk.corpus import wordnet
from torch.autograd import Variable
import re
import numpy as np
from Batch import nopeak_mask
import matplotlib.pyplot as plt
import seaborn
from Tokenize import tokenize


def get_synonym(word, SRC):
    syns = wordnet.synsets(word)
    for s in syns:
        for l in s.lemmas():
            if SRC.vocab.stoi[l.name()] != 0:
                return SRC.vocab.stoi[l.name()]
    return 0


def multiple_replace(dict, text):
    # Create a regular expression  from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))

    # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)


def translate_sentence(sentence, model, opt, SRC, TRG):
    model.eval()
    indexed = []
    sentence = SRC.preprocess(sentence)
    for tok in sentence:
        if SRC.vocab.stoi[tok] != 0 or opt.floyd is True:
            indexed.append(SRC.vocab.stoi[tok])
        else:
            indexed.append(get_synonym(tok, SRC))
    sentence = Variable(torch.LongTensor([indexed]))
    if opt.device == 0:
        sentence = sentence.cuda()

    sentence = beam_search(sentence, model, SRC, TRG, opt)

    return multiple_replace({' ?': '?', ' !': '!', ' .': '.', '\' ': '\'',
                             ' ,': ','}, sentence)


def translate(opt, model, SRC, TRG):
    sentences = opt.text.lower().split('.')
    translated = []

    for sentence in sentences:
        translated.append(translate_sentence(sentence + '.', model, opt,
                                             SRC, TRG).capitalize())

    return (' '.join(translated))


def subsequent_mask(size):
    "Mask out subsequent positions."
    attn_shape = (1, size, size)
    subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
    return torch.from_numpy(subsequent_mask) == 0


def greedy_decode(model, src, src_mask, opt, max_len, start_symbol):
    print('src.dtype', src.dtype)
    print('src_mask.dtype', src_mask.dtype)
    e_output = model.encoder(src, src_mask).long()
    ys = torch.ones(1, 1).fill_(start_symbol).type_as(src.data)
    for i in range(max_len-1):
        print('e_output.dtype', e_output.dtype)
        print('src_mask.dtype', src_mask.dtype)
        print('ys.dtype', ys.dtype)
        print('src.data.dtype', src.data.dtype)

        outputs = torch.LongTensor([[start_symbol]]).cuda()
        if opt.device == 0:
            outputs = outputs
        trg_mask = nopeak_mask(1, opt)
        # out = model.decoder(e_output, src_mask,
        #                     Variable(ys),
        #                     Variable(subsequent_mask(ys.size(1))
        #                              .type_as(src.data)))
        print('outputs.dtype', outputs.dtype)
        print('e_output.dtype', e_output.dtype)
        print('src_mask.dtype', src_mask.dtype)
        print('trg_mask.dtype', trg_mask.dtype)
        out = model.out(model.decoder(outputs,
                        e_output, src_mask, trg_mask))
        out = F.softmax(out, dim=-1)
        probs, ix = out[:, -1].data.topk(opt.k)
        # prob = model.generator(out[:, -1])
        print('ix[0]', ix[0])
        _, next_word = torch.max(probs, dim=1)
        next_word = next_word.data[0]
        ys = torch.cat([ys,
                        torch.ones(1, 1).type_as(src.data).fill_(next_word)],
                       dim=1)
    return ys


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-load_weights', default="weights_3")
    parser.add_argument('-k', type=int, default=6)
    parser.add_argument('-max_len', type=int, default=200)
    parser.add_argument('-d_model', type=int, default=512)
    parser.add_argument('-n_layers', type=int, default=6)
    # parser.add_argument('-src_data', required=True)
    # parser.add_argument('-trg_data', required=True)
    parser.add_argument('-max_strlen', type=int, default=200)
    parser.add_argument('-batchsize', type=int, default=2200)
    parser.add_argument('-src_lang', default="en")
    parser.add_argument('-trg_lang', default="fr")
    parser.add_argument('-heads', type=int, default=8)
    parser.add_argument('-dropout', type=int, default=0.11)
    parser.add_argument('-no_cuda', action='store_true')
    parser.add_argument('-floyd', action='store_true')

    opt = parser.parse_args()

    opt.device = 0 if opt.no_cuda is False else -1

    assert opt.k > 0
    assert opt.max_len > 10

    SRC, TRG = create_fields(opt)
    model = get_model(opt, len(SRC.vocab), len(TRG.vocab))
    t_src = tokenize(opt.src_lang)
    t_trg = tokenize(opt.trg_lang)

    model.eval()

    sent = t_src.tokenizer('Is English easier than French?')

    src = torch.LongTensor([[SRC.vocab.stoi[w] for w in sent]])
    src = Variable(src, requires_grad=False).cuda()
    src_mask = (src != SRC.vocab.stoi["<pad>"]).unsqueeze(-2).cuda()
    # out = greedy_decode(model, src, src_mask, opt,
    #                     max_len=80, start_symbol=TRG.vocab.stoi["<sos>"])
    out = beam_search(src, model, SRC, TRG, opt)
    print(sent)
    print(out)
    # print("Translation:", end="\t")
    # trans = "<sos> "
    # for i in range(1, len(out)):
    #     print('i', i)
    #     sym = TRG.vocab.itos[out[i]]
    #     if sym == "<eos>": break
    #     trans += sym + " "
    # print(trans)

    tgt_sent = out.split()
    def draw(data, x, y, ax):
        seaborn.heatmap(data,
                        xticklabels=x, square=True, yticklabels=y, vmin=0.0, vmax=1.0,
                        cbar=False, ax=ax)

    # for layer in range(0, 3):
    #     fig, axs = plt.subplots(1, 9, figsize=(15, 4))
    #     print("Encoder Layer", layer+1)
    #     for h in range(8):
    #         draw(model.encoder.layers[layer].attn.scores[0, h].detach().cpu().numpy(),
    #              sent, sent if h == 0 else [], ax=axs[h])
    #     draw(model.encoder.layers[layer].attn.scores[0, :].detach().cpu().numpy().max(axis=0),
    #          sent, [], ax=axs[8])
    #     plt.show()

    for layer in range(0, 3):
        # fig, axs = plt.subplots(1, 9, figsize=(15, 4))
        # print("Decoder Self Layer", layer+1)
        # for h in range(8):
        #     draw(model.decoder.layers[layer].attn_1.scores[0, h]
        #          .data[:len(tgt_sent), :len(tgt_sent)]
        #          .detach().cpu().numpy(),
        #          tgt_sent, tgt_sent if h == 0 else [], ax=axs[h])
        # draw(model.decoder.layers[layer].attn_1.scores[0, :]
        #      # .data[:len(tgt_sent), :len(tgt_sent)]
        #      .detach().cpu().numpy().max(axis=0)[:len(tgt_sent), :len(tgt_sent)],
        #      tgt_sent, [], ax=axs[8])
        # plt.show()
        print("Decoder Src Layer", layer+1)
        fig, axs = plt.subplots(1, 9, figsize=(15, 4))
        for h in range(8):
            draw(model.decoder.layers[layer].attn_2.scores[0, h]
                 .data[:len(tgt_sent), :len(sent)].detach().cpu().numpy(),
                 sent, tgt_sent if h == 0 else [], ax=axs[h])
        draw(model.decoder.layers[layer].attn_2.scores[0, :]
             # .data[:len(tgt_sent), :len(sent)]
             .detach().cpu().numpy().max(axis=0)[:len(tgt_sent), :len(sent)],
             sent, [], ax=axs[8])
        plt.show()

    # while True:
    #     opt.text = input("Enter a sentence to translate (type 'f' to load \
    #                      from file, or 'q' to quit):\n")
    #     if opt.text == "q":
    #         break
    #     if opt.text == 'f':
    #         fpath = input("Enter a sentence to translate (type 'f' to load \
    #                       from file, or 'q' to quit):\n")
    #         try:
    #             opt.text = ' '.join(open(opt.text, encoding='latin1')
    #                                 .read()
    #                                 .split('\n'))
    #         except:
    #             print("error opening or reading text file")
    #             continue
    #     phrase = translate(opt, model, SRC, TRG)
    #     print('> ' + phrase + '\n')

if __name__ == '__main__':
    main()
